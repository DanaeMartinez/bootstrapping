## Bootstrapping for Inferential Statistics

Bootstrap is a computer-based method for statistical inference without relying on assumption about the theorical distribution of the data.
With Bootstrap methods we can then estimate sampling distributions, perform confidence intervals and hypothesis testing just from only one sample data.
This repo conteins the following jupyter notebooks:

### 1. Parametric statistics

Introduction to the most common probability distributions 

- Introduction to graphical and quantitative exploratory data analysis
- Introduction to discrete and continuous random variables
    - Bernouilli distribution
    - Relationship between Binomial and Poisson distributions
    - Normal and exponential random variables   
- Random number generation 

### 2. Bootstrapping methods II
Parameter estimation by optimization and confidence intervals 

### 3. Bootstrapping methods III
Hypothesis testing

### 4. Case study

Analysis of the [Darwin's finches on Daphne Major Island](https://datadryad.org/stash/dataset/doi:10.5061/dryad.g6g3h) dataset 
- Exploratory data analysis (EDA)
- Parameter estimatimation 
- Hypothesis testing and p-value computing 
- Linear regressions
- Pearson correlation estimation

## Sources 

1. https://towardsdatascience.com/bootstrapping-for-inferential-statistics-9b613a7653b2
2. https://learn.datacamp.com/courses/statistical-thinking-in-python-part-1
3. https://learn.datacamp.com/courses/statistical-thinking-in-python-part-2